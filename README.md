# Readme of ZigURust blog

This is my blog where I write about Zig and Rust.

This blog is powered by [zola](https://www.getzola.org/). If ypu want to build it for youself, you need to clone this repository, install zola and use the zola manual to build it.

The theme used for this blog is [terminimal](https://www.getzola.org/themes/zola-theme-terminimal/).
