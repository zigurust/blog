+++
title = "Imprint"
date = 2022-12-15
weight = 1  
+++

## Contact information

* email: huntrss@posteo.me
* gitlab: 
	* [huntrss](https://gitlab.com/huntrss)
	* repository of this blog: [ZigURust/blog](https://gitlab.com/zigurust/blog)
* github: [huntr3ss](https://github.com/huntr3ss)
* linkedin: [Philipp Werner](https://www.linkedin.com/in/philipp-werner-Rust)

## Contact address

```
Philipp Werner
Anne-Frank-Weg 7
71282 Hemmingen
Deutschland
```

## Data protection declaration

This is a static website - a blog - which does ***not*** collect any data for any purposes. However it is hosted as a [GitLab page](https://docs.gitlab.com/ee/user/project/pages/). This means it is hosted by using the infrastrcuture provided by GitLab.

I *assume* that GitLab does collect data in a Server log, this means data that can be described as access data. This includes data as listed below:

* Which page was accessed (e.e. this blog)
* The time of the access
* Amount of data in bytes that were sent
* Link or source that referred to this site
* Used browser
* Used operating system
* Used IP address

As mentioned above, I ***will not*** use any of the above mentioned data. Nor can I use them, since I don't have any access to them.

Find further information about GitLab's privacy statement [here](https://about.gitlab.com/privacy/).

## License & copyright

All blog entries are licensed under [CC BY 4.0](https://about.gitlab.com/privacy/).

All the content of the blog is usually written by me, or quoted with a link to the original source. If you find something that is not correctly quoted of that you think does violate any copyright, please contact me and I can and will correct any mistake I did.

I will link to other websites, blogs etc.. I am not responsibly for the content of these links nor is the content of these links under the same license as this blog. Please check the links, their license and their content your self if you want to work in any way with the linked content.

