+++
title = "About"
date = 2022-12-30
weight = 0
+++

This blog is my blog where I try to deepen my knowledge about [Zig](https://ziglang.org/) and [Rust](https://www.rust-lang.org/). I do appreciate both languages and don't want to focus on one of both languages. At least at the moment. I hope you'll find some blog posts (in the future or already) that explain in more details what I do like about both languages and why it is hard for me to only choose one of both.

However I don't intent to pit both languages against each other. In some blog posts I certainly will compare both languages or conclude which language I did prefer, but it's not my focus.

Instead I will try to implement concise concepts and ideas in both languages (maybe sometimes using only one language) to learn syntax and concepts as well as possible limitations better.

So for whom is this blog? I believe that everyone who likes Zig or Rust or both languages. And everyone who wants to see some ins and outs of both languages, their similarities and differences in more details.

I'll hope you enjoy my blog and thank you for reading.

## What about the name?

It's obviously a mixing of Zig and Rust and it sounds like or is close to [Ziggurat](https://en.wikipedia.org/wiki/Ziggurat). And yes, I am that simple that I find that funny ;)
