+++
title = "Three things I like about Zig and Rust"
date = 2022-12-29
+++

# Three things I like about Zig and Rust

In this article I want to describe the three things I like about each of the
languages (Zig and Rust) the most. This means the 3 things I would name first,
if I would be asked to name these.

This article is organized in a way, that the things I like are ordered from
the most important to the slightly less important. Imagine if I would be asked
to name only one thing. I describe these things in the first section of each
language. If I would be allowed to name only 2 things, than it's obviously the
already mentioned thing as well as the one described in the next section. And
finally if I am asked to name 3 things, then ... I guess you know where this
is heading.

## The most important thing I like about Zig

Its simplicity. I like how simple this language is. For me (as still a beginner)
it feels like writing a script language (think JavaScript) although its a system
programming language. I experienced several times, after I fixed a compile
error, that I felt like I understood now, why I fixed the error. I had my fair
share with Rust (and especially C++) where I fixed the compile error but did not
really grasp why (could be a testament to my programming skills in general ;)).
And yes Zig has its complex concepts. I think `comptime` is not trivial and I
currently lack the understanding of how the `async` story in Zig actually works.
However it is way less complex than C++ and also as Rust. It has less syntax
and less "magic" like operator overloading (looking at you C++) or macros (which
are - in my opinion - a doubled-edged sword in every language). It simplifies
reading code, like a lot, and also understanding what is going on.

What I actually like about this simplicity is, that it lowers the barrier
to actually start writing code. At least for me it makes it simpler to start
writing code in this beautiful but powerful language since I can remember most
of the syntax I need in may head and most of its syntax feels very intuitive.

Simplicity is also the thing I feel that Rust ist not good at. I do understand
that Rust chooses to evolve and to provide better and more powerful and
ergonomic syntax but it also means it adds more "things" with each improvement.
This means when you read Rust code, you may need to be familiar with newer
and more powerful and ergonomic syntax as well as the "legacy" syntax. Usually
I like every addition to Rust (the ones I can understand at least) and find
these additions usually pretty well thought through. And its not and individual
improvement that makes the language more complex, its the accumulation of all
these improvements. At least I have this bad feeling in my stomach every time
I read about a new and exciting new feature in Rust. However, I also believe
that part of Rust's complexity stems from its safety story, or simplified said
the borrow checker. Which directly brings us to the first thing I do like about
Rust.

## The most important thing I like about Rust

The borrow checker. For me the borrow checker is what does make Rust more of
a revolution than an evolution. And I know, there are very good statical code
analysis tools for C and C++. But you need to opt-in for your projects. With
Rust's borrow checker you can only opt-out for certain `unsafe` blocks and
functions and you don't event disable the borrow checker completely by using
`unsafe`. Reviewing `unsafe` blocks with more scrutiny than the rest of the
code is very beneficial and will hopefully prevent over-extensive usage of
`unsafe`. I really appreciate the "safe" feeling I get, when using Rust and the
experience, that "when it compiles it works" seems achievable.

As mentioned above, this comes with a price of more complexity that Rust has,
but it's seems a fair bargain for me. Maybe Zig finds a solution to keep its
simplicity and provide its own version of the borrow checker. For now, I really
miss it in Zig because a find it so refreshingly genuine and relevant.

I can remember a C++ (11 and 14) training I did in 2017. Although I did not
really understood the concept of moving a variable very well (definitely due
to my incompetence but I also think the trainer didn't teach this concept very
well) I got the creeps when the trainer mentioned that accessing a variable
after it is moved is undefined behavior. I immediately thought, how can I
remember this and how do I prevent it. When I learned Rust, I started to
understand the concept of moving a variable at last, and I understood how I can
prevent the problem of accessing a moved variable: By using Rust and its borrow
checker.

And as so many before me and after me, I started learning Rust by fighting the
borrow checker at first but now I usually work with the borrow checker. If the
borrow checker reports and error, I am not annoyed anymore, I am alerted. That's
what I find so powerful. If I would write a complex software product, maybe
with safety relevance, then I prefer a language like Rust over other languages.
Simply because the borrow checker prevents the team members as well as myself
from severe errors (modulo bugs in the borrow checker). Obviously Rust is not
ready for safety relevant software as for the writing of this article but I
hope we will eventually get there. One relevant step will be the [Ferrocene
Rust compiler tool chain](https://ferrous-systems.com/ferrocene/). I am looking
forward to the first release.

## The second thing I like about Zig

No hidden allocations as described in [Zig - in-depth - Manual Memory
Management](https://ziglang.org/learn/overview/#manual-memory-management). It
seems as something not that important but I find it very useful and important.
It means that using a Zig library in an environment (e.g. embedded systems)
which does have very strict requirements on dynamic memory, is simplified. One
can choose an allocator that fits the strict requirements. This assumes that
dynamic memory is allowed at all. But even in such cases, I can immediately see,
if I can use a specific function or type in such an environment. Additionally
one can use a specific allocator more optimized for the specific use case
in order to improve the performance. Or different allocators can be used in
different parts of the application. This is very flexible.

Yes, Rust will eventually get the [allocator traits and std::heap](https://
github.com/rust-lang/rust/issues/32838) and C++ does also enable to use
a specific allocator for example for [std::vector](https://cplusplus.com/
reference/vector/vector/). However these are opt-in and default to using the
default allocator. I personally find it a huge difference if you explicitly
require an allocator or if it is an option. The explicitness does reduce the
ergonomics of the API that is for sure, but it does not hide an important aspect
of the behavior of the API: if or if not it requires dynamic memory. I expect
that libraries written in Zig will keep this explicitness regarding allocations.
Since Rust does (as of writing this article) not support defining allocators for
collections and since it will be opt-in as in C++, I think more libraries will
be less explicit about memory allocations or providing the possibility to hand-
over an own allocator.

Therefore, it blew my mind, when I saw that this is so explicit in Zig.

## My number two thing for Rust

Rust's toolchain is really, really good. I specifically mean [cargo](https://
doc.rust-lang.org/cargo/), including [crates.io](https://crates.io/) (or the
possibility to use alternative registries), as well as the tools I can use
or add to cargo. I think about [clippy](https://github.com/rust-lang/rust-
clippy), [criterion](https://bheisler.github.io/criterion.rs/book/index.html)
and format and more. What I do find very, very powerful are doc tests. This
means you can write documentation of your APIs including an example. This makes
a documentation already ok-ish. But you can also test, that your documentation
is still correct (compiles and the usage does work as asserted) as well as
testing your code. Doc tests may not be sufficient for production ready code.
But if you write an open source library, in your free time, as a one-person-
show, than using doc tests, maybe together with [code coverage](https://lib.rs/
crates/cargo-llvm-cov), is already pretty good quality-wise (regarding the
circumstances). And doing this is very easy, and without too much overhead or
hassle. I am still amazed. I hope that Zig will get there as well (especially
doc tests). There may be other languages with such a great tool chain
unfortunately I don't know any language with a tool chain as good as Rust's.

## Number three for Zig

I find `comptime` very powerful and useful. With `comptime` you do get
the possibility for generic programming, meta programming and compile time
evaluation. I can do all of these things using more or less the same syntax
as with other source code parts. There are some limitations and its not all
perfect, but I find it a very compelling and good solution. As I have an
embedded software background, I especially do appreciate the compile time
evaluation capabilities of `comptime`.

Rust and C++ can also do generic programming (generics in Rust, templates in C+
+), meta programming (macros in Rust and C++, template meta programming in C++)
and compile time evaluation (`const fn` in Rust, `constexpr` in C++). I find the
solution in Zig preferable then the solution in Rust. I am less experienced with
C++ and not a big fan of C++, therefore I omit the arguments why I do like Zig's
solution over C++ solution.

So here is why I like Zig's `comptime` more the solution used in Rust.

First of all, generic programming with generics in Rust is great. I may even
prefer Rust's solution here over Zig's.

Regarding meta programming: I like the concept of doing meta programming
in the language itself as opposed to using Rust's macro rules macros. Yes,
Rust's macros (macro rules and proc macros) are compelling and a useful and I
understand and appreciate the necessity to have a tool to extend the language
for missing features. But it is also a gateway to introducing domain specific
languages (DSL) to libraries and frameworks. And why not, it is so much fun
to create DSLs and it can improve the ergonomics of an API. However, it does
also put another mental burden on the users of the API, because they need
to learn another language to use the API. I learned about this problem from
reading matklad's blog post [For the love of macros - domain specific languages]
(https://matklad.github.io/2021/02/14/for-the-love-of-macros.html#Domain-
Specific-Languages). I am not sure, if it is possible to create a DSL with
Zig's `comptime` as well since strings, or more precise `[] const u8` could be
processed (maybe I write a blog post about this in the future). But if I compare
macro rules macros (which are part of Rust) to `comptime` meta programming, I
do have a different and specific  - and sometimes not that easy to understand -
syntax in macro rules macros or more or less the syntax I program the rest of my
source code.

Finally `const fn` in Rust is getting better with every release and looses
a lot of its limitations it had when I explored them in more details in 2020
(see [Generating static arrays during compile time in Rust](https://dev.to/
rustyoctopus/generating-static-arrays-during-compile-time-in-rust-10d8). From my
experience with `comptime` so far, there seem to be very few limitations, but I
want to explore this in more detail in a future blog post.

So in comparison to Rust, this third thing I like about Zig may not clearly
better than Rust's solution, but I still do like `comptime` very much.

## The third thing I like about Rust

Rust's strong type system. Sure Zig is also strongly typed (and Zig's type
inference is very, very good) but in Rust I can write code that cannot be
executed in a multi-threaded environment, because the types that are used do
not implement [Send](https://doc.rust-lang.org/std/marker/trait.Send.html) and
[Sync](https://doc.rust-lang.org/std/marker/trait.Sync.html). This is so well
described in this blog post by cliffle: [Safely writing code that isn't thread-
safe](http://cliffle.com/blog/not-thread-safe/). I find this very impressive and
useful. I can use Rust's type system to express invariants and let the compiler
check them for correctness. I believe (and hope) that some of this is also
possible in Zig (and I will try to write another blog post about it) but for
the moment, it is the most powerful and expressive type system I've ever written
code in it. And I love it.

## Conclusion

I hope I was able to express my three things about both languages I do like the
most and why I like them. I guess during my journey to deepen my knowledge about
these languages I will find out that I misunderstood or misinterpreted some of
these things and may need to updated or rebuke my own article ;). I also do hope
that I could express, why I love both languages and why I find it so hard to
just learn one of these languages.

Discussion on
[reddit](https://www.reddit.com/user/huntrss/comments/zzwlpk/three_things_i_like_about_zig_and_rust/).
