+++
title = "Webdemos in Zig"
date = 2023-07-26

+++

I ported the webdemo tutorial from [cliffle](http://cliffle.com/) to Zig: [Making really tiny WebAssembly graphics demos](http://cliffle.com/blog/bare-metal-wasm/). This great tutorial explains how to create demos in WebAssembly in Rust, which I recommend you to read.

You can find my template / example code in this repository: [webdemos](https://gitlab.com/zigurust/webdemos).

The rest or the post I want to: 

* provide my motivation for porting the tutorial, 
* give an overview of the template / example projects
* showcase an example webdemo on this very page
* explain some details regarding the implementation in Zig
* finalize in a conclusion and provide a small comparison to the Rust implementation

I hope you enjoy the post and you create some webdemos in Zig yourself.

## Motivation

I personally do like WebAssembly (WASM). I find it a very intriguing, in the browser as well as in non-web use cases. I especially like the security concepts it provides: A WebAssembly needs to define the interfaces it requires very explicit. This is a good thing, although it is not a security fairy dust. Since I like this technology I enjoy to learn more about it by using it in my personal projects.

Additionally I work in embedded software professionally, and I like the resource limitations (memory, storage, CPU) and the usage of third party software or standard libraries is usually very restricted or even not possible. But this can be fun, since it requires one to learn some of the most basic concepts, algorithms etc..

I think that WebAssembly (without WASI or a standard library) does provide a similar experience, but it is easier to get something on the screen: The only "thing" that is required is a WebAssembly-capable browser: No embedded hardware, no flash software, no qemu etc.. I think also that demos, or webdemos in this case, do also provide this resource limitations experience that embedded software does require.

Finally I studied mathematics (Technomathematics to be precise) and therefore I always have an interest in topics with a math background: Computer graphics and especially computer graphics for demos are interesting for me.

## Zig compiler version

All webdemos were created with the Zig compiler in version 0.10.1. Since August 3rd 2023 Zig is in version 0.11.0. I think that most of the code and more important, concepts I discuss are still valid and worth reading. However if you want to use a newer version of Zig, the demos may need to be modified accordingly.

## Overview

In the [webdemos](https://gitlab.com/zigurust/webdemos) repositories. You can find 4 projects that can be used as a template or an example:

* mininmal_template: Provides a WASM module that can add two `i32` numbers. The `index.html` file loads the module, adds two numbers and writes the result to console. It provides the minimal template for a WebAssembly module in Zig.
* minimal_graphics: Is the minimal module that provides graphical output. Still static but a canvas is displayed in the `index.html` with magenta color. The buffer for this image is created and modified in the WASM module.
* tartan: Here a structure is created and not just a simple one-colored image.
* animation: Finally this module does create an animated image, which is exactly what we want. The animation example (with a smaller canvas) is provided below:

<script type="module">
      async function init() {
        const { instance } = await WebAssembly.instantiateStreaming(
          fetch("./animation.wasm")
        );

        const width = 600;
        const height = 100;

        const canvas = document.getElementById("demo-canvas");
        canvas.width = width;
        canvas.height = height;

        const buffer_address = instance.exports.buffer.value;
        const image = new ImageData(
            new Uint8ClampedArray(
                instance.exports.memory.buffer,
                buffer_address,
                4 * width * height,
            ),
            width,
        );

        const ctx = canvas.getContext("2d");

        const render = () => {
          instance.exports.go();
          ctx.putImageData(image, 0, 0);
          requestAnimationFrame(render);
        };

        render();
      }

      init();
</script>
<canvas id="demo-canvas"></canvas>

## Build.zig 

In order to create a webdemos WASM module, I created a Zig library project via `zig init-lib`. I added the wasm32 and freestanding as the default target in `build.zig`:

```zig
// in build.zig
const wasm_target = std.zig.CrossTarget{ .os_tag = .freestanding, .cpu_arch = .wasm32 };
const target = b.standardTargetOptions(.{ .default_target = wasm_target });
```

I also defined to create a shared library instead of a static library:

```zig
// in build.zig
const lib = b.addSharedLibrary(module_name, "src/main.zig", std.build.LibExeObjStep.SharedLibKind.unversioned);
```

In order to copy the created WASM module parallel to my `index.html` which I want to serve statically, I also added a system command:

```zig
// in build.zig
const cp_cmd = b.addSystemCommand(&[_][]const u8{ "cp", path_to_module, "./static/" });
b.getInstallStep().dependOn(&cp_cmd.step);
```

You can find the complete `build.zig` of the animation example [here](https://gitlab.com/zigurust/webdemos/-/blob/01450f4c938fcf7d08b4df2465113712b45874b9/animation/build.zig).

## Atomics

The original Rust implementation of the animation demo used an atomic to store the current frame number. From my understanding this is not strictly necessary, since the webdemo is run in a single thread in the browser. Using atomics in Rust for `static mut` variables generally makes sense, since access to `static mut` may result in a data race when executed in a multi-threading environment. Zig is less strict with regards to static variables, but I thought, when I am already here, why not find out how atomics work in Zig.

Zig has a generic atomic type in the standard library: [std.atomic.Atomic](https://ziglang.org/documentation/0.10.1/std/#root;atomic.Atomic), while Rust's atomic types are currently more explicit, e.g. [std::sync::atomic::AtomicU32](https://doc.rust-lang.org/std/sync/atomic/struct.AtomicU32.html). Methods on an atomic require a memory ordering, which are pretty much the same, besides that Zig uses [Ordering.Monotonic](https://ziglang.org/documentation/0.10.1/std/#root;atomic.Ordering) and Rust uses [Ordering.Relaxed](https://doc.rust-lang.org/std/sync/atomic/enum.Ordering.html). From what I understood after reading [this](https://kprotty.me/2021/04/08/understanding-atomics-and-memory-ordering.html) great article, relaxed and monotonic are the same, and it is a memory ordering that makes sense if synchronization is only relevant for a single variable and not between multiple variables.

An atomic in Rust does also provide the [fetch_add](https://doc.rust-lang.org/std/sync/atomic/struct.AtomicU32.html#method.fetch_add) method which I didn't find in Zig's Atomic implementation. So I used an idea I've found in Mara Bos's great book [Rust Atomics and Locks](https://marabos.nl/atomics/) in chapter 3 regarding an lazy [initialization example](https://marabos.nl/atomics/atomics.html#example-racy-init) of compare and exchange. Zig's atomics do provide [compareAndSwap](https://ziglang.org/documentation/0.10.1/std/#root;atomic.Atomic.compareAndSwap) (beyond others) which I used to come up with this solution:

```zig
// increment the frame number
const current_frame_number = frame.load(.Monotonic);
const new_frame_number = current_frame_number + 1;
// Should not happen in the demo, but in a different setting another concurrent thread may already have incremented in the frame number
const opt_frame_number = frame.compareAndSwap(current_frame_number, new_frame_number, .Monotonic, .Monotonic);
var frame_number = new_frame_number;
// If the swap was not necessary, i.e. the frame_number that was loaded differs from the actual frame number, use the number stored in the atomic
if (opt_frame_number) |n| {
  frame_number = n;
} 
```

I am not sure about the correctness of the solution, so let me know if there is a mistake.

Zig does also provide compiler builtins for atomic operations, for example the [@cmpxchgStrong](https://ziglang.org/documentation/0.10.1/#cmpxchgStrong) which is actually used in the implementation of [compareAndSwap](https://ziglang.org/documentation/0.10.1/std/#root;atomic.Atomic.compareAndSwap). There is also [@atomicRmw](https://ziglang.org/documentation/0.10.1/#atomicRmw) I would like to use, in order to have something similar to [fetch_add](https://doc.rust-lang.org/std/sync/atomic/struct.AtomicU32.html#method.fetch_add) but I am not sure how to use it properly or correctly. And I am quite happy with my solution, especially since in the absence of multi-threading, there should not be any race conditions regarding updating the frame number.

## WASM module size comparison

Zig, as well as other languages like Rust and C, provides the possibility to build your libraries and executables optimized for size (in Zig 0.10.1 this can be achieved with the `-Drelease-small=true` option). This comes in handy for WASM modules and especially interesting for this webdemos use case. In the original webdemos in Rust tutorial blog [post](http://cliffle.com/blog/bare-metal-wasm/) the WASM module sizes were optimized for size. So I created this section for comparison between the WASM module sizes in both languages (at least the ones that are identical in functionality). Since the article is from 2019, the comparison is not fair, since the situation for Rust with a newer tool chain may have improved. So take the results with this in mind and don't interpret too much into them. I also added the size of the animation webdemo, that was optimized further with *"arcane techniques"* to be only 88 bytes, see [Web Demos](http://cliffle.com/p/web-demos/).

|[WASM module]|[Rust]|[Rust (arcane)]|[Zig]|
|:------------|-----:|--------------:|----:|
|tartan|189|n/a|173|
|animation|213|88|200|

In comparison, the animation module build with speed optimization in Zig (i.e., `-Drelease-fast=true`) has the size of 5.6 kilobytes. But according to `wasm-objdump` (see [WABT](https://github.com/WebAssembly/wabt)) it also comes with multiple custom sections like `.debug_info` or `.debug_line` which make up the largest junk of this WASM module. The WASM module build with size optimization does not contain these sections.

Below are the sections of the `wasm-objdump` of the small build:

```shell
     Type start=0x0000000a end=0x0000000e (size=0x00000004) count: 1
 Function start=0x00000010 end=0x00000012 (size=0x00000002) count: 1
   Memory start=0x00000014 end=0x00000017 (size=0x00000003) count: 1
   Global start=0x00000019 end=0x0000002a (size=0x00000011) count: 2
   Export start=0x0000002c end=0x00000044 (size=0x00000018) count: 3
     Code start=0x00000047 end=0x000000c8 (size=0x00000081) count: 1
```

And here the sections for the fast build:

```shell
     Type start=0x0000000a end=0x0000000e (size=0x00000004) count: 1
 Function start=0x00000010 end=0x00000012 (size=0x00000002) count: 1
   Memory start=0x00000014 end=0x00000017 (size=0x00000003) count: 1
   Global start=0x00000019 end=0x0000002a (size=0x00000011) count: 2
   Export start=0x0000002c end=0x00000044 (size=0x00000018) count: 3
     Code start=0x00000047 end=0x000000ef (size=0x000000a8) count: 1
   Custom start=0x000000f2 end=0x000007b8 (size=0x000006c6) ".debug_info"
   Custom start=0x000007bb end=0x00000988 (size=0x000001cd) ".debug_pubtypes"
   Custom start=0x0000098b end=0x00000a60 (size=0x000000d5) ".debug_loc"
   Custom start=0x00000a63 end=0x00000ba8 (size=0x00000145) ".debug_abbrev"
   Custom start=0x00000bab end=0x00000d2a (size=0x0000017f) ".debug_line"
   Custom start=0x00000d2d end=0x00001377 (size=0x0000064a) ".debug_str"
   Custom start=0x0000137a end=0x0000157d (size=0x00000203) ".debug_pubnames"
   Custom start=0x0000157f end=0x0000159f (size=0x00000020) "name"
   Custom start=0x000015a1 end=0x000015bb (size=0x0000001a) "producers"
```

In the sections that both modules have, there is only a difference in the size of the code section: 129 bytes (small) to 168 bytes (fast). Zig provides also the possibility to strip when building libraries, objects or executables (`-fstrip` as option to `zig build-lib`, `zig build-obj` and `zig build-exe`). This can also be achieved by setting the `strip` field in a `build.zig` file on your [LibExeObjStep](https://ziglang.org/documentation/0.10.1/std/#root;build.LibExeObjStep) object. See example below:

```zig
// build.zig in the animation example
const lib = b.addSharedLibrary(module_name, "src/main.zig", std.build.LibExeObjStep.SharedLibKind.unversioned);
lib.setTarget(target);
lib.setBuildMode(mode);
// strip debug symbols
lib.strip = true;
lib.install();
```

Doing so results in a `-Drelease-fast` WASM module with 239 bytes instead of 5.6 kilobytes. The `wasm-objdump` then looks like this:

```shell
     Type start=0x0000000a end=0x0000000e (size=0x00000004) count: 1
 Function start=0x00000010 end=0x00000012 (size=0x00000002) count: 1
   Memory start=0x00000014 end=0x00000017 (size=0x00000003) count: 1
   Global start=0x00000019 end=0x0000002a (size=0x00000011) count: 2
   Export start=0x0000002c end=0x00000044 (size=0x00000018) count: 3
     Code start=0x00000047 end=0x000000ef (size=0x000000a8) count: 1
```

This means the aforementioned custom sections have been stripped, as expected.

## Tool chain

What I particular liked about the Zig experience is that `-Drelease-small` does produce small WASM modules without further processing. In Rust, which has a great WASM tooling and ecosystem, one must still use `wasm-strip` and `wasm-opt` to create a small binary.

## Conclusion

I liked porting the webdemos from Rust to Zig and did learn a lot doing so. I may be creating or porting some webdemos in the future or use the setup (i.e. Zig + WASM + Browser) for further low-level-like concept implementations.

## Discussion

Engage in a discussion on [ziggit.dev](https://ziggit.dev/t/webdemos-in-zig/1469).

