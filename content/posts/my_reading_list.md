+++
title = "My reading list"
date = 2023-01-02
+++

# Reading list

This is my reading list regarding the books I want to read and which I read.
The books listed are not necessary Zig or Rust related.

## Ranked books

This is the list of books I want to read and which I have an order. The 
order may change and read books are removed from this list.

### 1. Rust Atomics and Locks

* by Mara Bos
* Released January 2023
* Publisher(s): O'Reilly Media, Inc.
* ISBN: 9781098119447

### 2. Rust for Rustaceans

* Idiomatic Programming for Experienced Developers
* by Jon Gjengset
* November 2021
* ISBN-13: 9781718501850

### 3. The Art of Readable Code
* by Dustin Boswell, Trevor Foucher
* Released November 2011
* Publisher(s): O'Reilly Media, Inc.
* ISBN: 9780596802295

## Unranked books

Unranked books are without any order. I did not take the time to rank them at
the moment.

### The Mythical Man-Month

* Essays on Software Engineering, Anniversary Edition, 2nd Edition
* by Frederick P. Brooks
* Released August 1995
* Publisher(s): Addison-Wesley Professional
* ISBN: 9780201835953

### Working Effectively with Legacy Code

* by Michael Feathers
* Released September 2004
* Publisher(s): Pearson
* ISBN: 9780131177055

### Seven Concurrency Models in Seven Weeks

* When Threads Unravel
* by Paul Butcher
* Published: July 2014
* Publisher: Pragmatic Bookshelf
* ISBN: 9781937785659

## Data-Oriented Design

* software engineering for limited resources and short schedules
* by Fabian Richard
* Released September 2018
* Publisher: Richard Fabian
* ISBN-13: 978-1916478701
* [Data oriented design](https://www.dataorienteddesign.com/dodbook/)

## Read books

### Apprenticeship Patterns

* by Dave Hoover, Adewale Oshineye
* Released October 2009
* Publisher(s): O'Reilly Media
* ISBN: 9781449379407

#### Conclusion: Apprenticeship Patterns

* Nice book that helped me to reflect about the current state of my career. 
* I may come back to it, to find ideas for side ideas etc..

### 97 Things Every Programmer Should Know

* by Kevlin Henney
* Released February 2010
* Publisher(s): O'Reilly Media, Inc.
* ISBN: 9780596809485

#### Conclusion: 97 Things Every Programmer Should Know

* I would recommend the book if you just started as a software engineer
* If you are experienced like me (10 years and above) then it is nice to check
  your knowlegde and see what you already know or not
* Most of the things you should know, if still found mostly correct or that I
  could agree to some extend
* Only "thing" that I would see differently today is the DRY (Don't Repeat 
  Yourself) principle. Yes it is still relevant and makes sense but DRY can
  increase coupling which should not be underestimated since it can lead to
  unmaintainable software because one wanted to don't repeat trivial concepts
  and bundled them together without too much cohesion. I think that domain
  driven design and to a certain extend microservice architecture demonstrate
  show that DRY may not be the most relevant aspect if you want to scale or 
  create maintainable software.
