+++
title = "Naive parallel map implementation in Rust and Zig"
date = 2023-03-16
+++

**Update 2023-03-16**: *I updated the post due to the great feedback from
[Nisogen](https://www.reddit.com/user/Nisenogen/) in the Reddit post I linked
below*

**Update 2023-03-16**: *I updated again, due to the great feedback from [superjoe30](https://www.reddit.com/user/superjoe30/)
in this [comment](https://www.reddit.com/r/rust/comments/11u1b8j/comment/jcma7zi/?utm_source=share&utm_medium=web2x&context=3)*

Since my studies (more than 10 years ago) I was always interested in
parallelism. With parallelism I mean data parallelism, this means solving a
single problem - represented with data - by using multiple units of works (e.g.
threads). This (hopefully) speeds up the execution time with respect to solving
the problem in a strict serial way (w.r.t. to CPU cores, threads etc., not
considering instruction parallelism like SIMD instructions). An example of such
a data parallel solution could be a parallel sort algorithm in contrast to sorting
with in a serial way. What I don't mean by data parallelism is explicitly task
parallelism. An example for task parallelism can be a web server that serves
multiple independent requests in parallel (as opposed to handling one request
by using multiple unit of works, like threads).

During 2017, I had the opportunity to learn about parallel programming patterns
through the book *Structured Parallel Programming* from McCool, Reinders, Robison
(eBook ISBN: 9780123914439, Paperback ISBN: 9780124159938). The main idea of
these patterns are - as with other programming patterns - that, when implemented
correctly, that a program that uses these patterns can benefit from parallelization
while at the same time avoiding common pitfalls of parallel programming, especially
when threads are concerned.

In this blog post I describe the map pattern - probably the simplest but most
powerful of these patterns - and my naive implementations of this pattern in Rust
and Zig.

## Disclaimer

This is an educational blog post, no code posted here or in the linked repository
is production ready and should not be used outside of fun projects. The code and
my writings may contain errors and inefficiencies. See the reddit link below to
engage in a discussion. This article is for people who want to see some Rust and
Zig code and to learn some basics about parallel programming.

## TL;DR

The realization of the parallel map pattern with Rust was surprisingly
straight-forward, considering that a buffer needs to be shared between different
threads. Unsurprisingly, [rayon](https://crates.io/crates/rayon)'s
ergonomics and performance were great. Finally it is obviously possible to implement the map pattern in Zig.

The code can be found in the gitlab repository
[data parallelism](https://gitlab.com/zigurust/data_parallelism/-/tree/main/map).
It is licensed under [MIT](https://gitlab.com/zigurust/data_parallelism/-/blob/main/LICENSE).

## The map pattern explained

The map pattern is rather simple: To each individual element of the `from` buffer
(or array, or list, etc.) the kernel or mapping function is applied and the
result is copied into the matching element of the `to` buffer with the same index:

![Map pattern](map_pattern_explanation.drawio.png "Map pattern")

## Map parallelization strategy

As the parallelization strategy I chose to split the buffer into n parts and
let n threads process these parts in parallel:

![Map parallelization strategy](map_parallelization_strategy.drawio.png "Map parallelization strategy")

Ideally the parts have equal length. I chose the number of threads equal to
the number of cores (i.e., 4 threads on my laptop), respectively I used the
[`thread::available_parallelism`](https://doc.rust-lang.org/std/thread/fn.available_parallelism.html)
function in Rust.

There is a similar function in Zig:
[`std.Thread.getCpuCount`](https://ziglang.org/documentation/master/std/#A;std:Thread.getCpuCount)
(0.11.0-dev.1862+e7f128c20). However I needed to define the maximum number of
threads anyway (see more details below), therefore I didn't use it.

## Application structure

I've written several application in Rust and Zig with similar structure that
differ in the implementation details. However the applications have a similar
structure which I want to lay out in this section.

The main function of all applications follow these steps:

1. Tracing / logging is set-up
2. Two buffers are allocated (only one for the *first* version using rayon)
3. The map function is executed (time measurement does only consider this
   execution)
4. Finally the result is verified, just in case

As described above, the rayon version does only allocate one buffer, since
when using rayon, the result is later collected into a new buffer (e.g. a
`Vec`).

The calculation that is executed in each application is, the mapping of an
`f32` buffer of length 300 millions into a buffer of the same length by
applying the square root function to each element individually. I used the
square root functions implemented in the Rust standard library, respectively
the `@sqrt` built-in function in Zig.

## Rust implementations

In this section I will describe some details and "things" I learned in the Rust
versions I implemented.

### Rust interface of (almost) all versions

Besides the *first* rayon version (see more details below) all versions do have the
following interface:

```rust

fn map<U: Send + Sync, V: Send, F: Fn(&U) -> V + Send + Sync>(from: &[U], to: &mut [V], kernel: F) {
   /// ...
}
```

The `map` function takes a reference to the `from` buffer of generic `U` and a
mutable reference to the `to` buffer of generic type `V` and the kernel function.
This is the function that maps each element of the `from` buffer to matching
element of the `to` buffer. All generic types realize the `Send` and `Sync` traits
since the elements in the buffer and the function are shared between different
threads in the parallel versions.

### Rust serial

The implementation of the naive serial version is pretty straightforward:

```rust
let length = min(from.len(), to.len());
for i in 0..length {
    to[i] = kernel(&from[i]);
}
```

The minimum length is calculated, this means the length of the smallest buffer.
This avoids a buffer overread or overwrite. In a `for` loop the kernel function
is applied to the elements of the `from` buffer and the result is written into
the `to` buffer.

### Rust serial unsafe

Due to the (unscientific) time measurements I did (see below) I created another
serial version but this time I access the buffers without bounds check in the loop:

```rust
for i in 0..length {
    let element = unsafe { to.get_unchecked_mut(i) };
    *element = kernel(unsafe { &from.get_unchecked(i) });
}
```

Since I use the minimum length of both buffers for this iteration, I think that
using the unchecked accesses is acceptable, but I may be mistaken. If you know
what I did oversee, please comment in the reddit post linked at the end of the
article.

### Rust naive

The serial versions were pretty simple, however the parallel version is
slightly more complex. Below are the steps needed for the parallel version:

* I use `std::thread::available_parallelism` to get the number of threads I
   want to use. Since this method returns a `Result<NonZeroUsize>` I use the
   value 1 as default. I guess if this code returns an error it is okay to
   to use 1 thread.

```rust
let max_threads = available_parallelism()
.unwrap_or(unsafe { NonZeroUsize::new_unchecked(1) })
.get();
```

* Then I calculate the length of each buffer used in each thread. In my case
   (300 million elements and 4 threads) the buffers can be split in parts of
   the same length. If this is not the case, then it would make sense that
   the last thread receives the biggest part of the buffer. The difference
   however should be small, since number of cores (or available parallelism)
   is not a huge number. I have 4 (but my laptop is 12 years old) but even
   newer machines may have 32 cores, which is not a huge difference if the
   affected buffers are large. But I do not really take this fact into
   account that the buffer length may not be divisible without reminder
   by the number of threads.

```rust
let len_per_thread = length / max_threads;
```

* Then I use scoped threads, by creating a scope with `std::thread::scope`.
   A scope automatically joins all threads spawned when the scope is closed.
   Using a scope did simplify the sharing of the kernel function with the
   threads. Otherwise a thread may outlive the kernel function but it may still
   be called. This means, without using a scope I could not share the kernel
   function with the spawned threads due to a borrow checker error. Inside the
   scope I spawn a thread up to `max_threads`.

```rust
scope(|s| {
   for i in 0..max_threads {
      // splitting of buffers happens here   
        s.spawn(move || {
            // calculation happens here
        });
    }
});
```

* In order to share the buffers, I create slices from the given `from` buffer
   and use `std::slice::split_at_mut` for the mutable `to` buffer. As mentioned
   before, I do not consider that the `max_threads` may not divide the buffers
   length without reminder, but it should certainly be possible to consider
   this. In order to split the `to` buffer, I use a mutable variable `rest`
   that is used to store the rest of the `to` buffer in each loop iteration.

```rust
let mut rest = to;
scope(|s| {
    for i in 0..max_threads {
        let from_part = &from[i * len_per_thread..(i + 1) * len_per_thread];
        let (to_part, res) = rest.split_at_mut(len_per_thread);
        rest = res;
        // ...
    }
});
```

* Finally I spawn each thread the following way:

```rust
s.spawn(move || {
   let len = min(from_part.len(), to_part.len());
   for i in 0..len {
      let u = &from_part[i];
      to_part[i] = kernel(u);
   }
});
```

### Rust naive unsafe

As with the serial version, I also created an unsafe version. It differs from
the safe version by using unchecked access to the buffers inside each threads
execution:

```rust
s.spawn(move || {
    let len = min(from_part.len(), to_part.len());
    for i in 0..len {
        let element = unsafe { to_part.get_unchecked_mut(i) };
        let u = unsafe { from_part.get_unchecked(i) };
        *element = kernel(u);
    }
});
```

Since I only iterate with respect to the length of the smallest buffer, I
believe it to be safe to do this. But let me know if I am wrong here (use the
reddit post linked at the bottom of this article).

### Rust rayon

The rayon version uses a different interface then the other Rust versions I wrote.
The reason lies in the nature of rayon, where you can chain multiple parallel
operations together before you finalize your operations, for example by
collecting it into a vector. The implementation of the rayon version is so
straight-forward and concise that I add it below together with the interface:

```rust
fn map<U: Send + Sync, V: Send, F: Fn(&U) -> V + Send + Sync>(from: &Vec<U>, kernel: F) -> Vec<V> {
   from.par_iter().map(kernel).collect::<Vec<V>>()
}
```

The interface does get a `from` buffer again and a kernel function but it returns the
`to` buffer instead of writing into it. In the implementation I use the `map`
method of rayon on the given buffer together with the kernel. I then collect
the result in a `Vec`. It doesn't get much easier then this.

## Rust rayon zipped

I added this version created and proposed in this [reddit comment](https://www.reddit.com/u/huntrss/comments/11u15q9/comment/jcm5gfk/?utm_source=share&utm_medium=web2x&context=3).
It zips together both buffers and uses the parallel `foreach` method of rayon:

```rust
fn map<U: Send + Sync, V: Send, F: Fn(&U) -> V + Send + Sync>(
    from: &Vec<U>,
    to: &mut [V],
    kernel: F,
) {
    let zipped_arrays = from.par_iter().zip(to);
    zipped_arrays.for_each(|(f, t)| *t = kernel(f));
}
```

This version uses rayon again, keeps the interface of all other Rust versions
besides the *first* rayon version. I'm astonished.


## Rust serial zipped

Similar to the Rust rayon zipped version, the buffers are zipped by using
`std::iter::zip` and then iterated:

```rust
fn map<U: Send + Sync, V: Send, F: Fn(&U) -> V + Send + Sync>(from: &[U], to: &mut [V], kernel: F) {
    let iter = std::iter::zip(from, to);
    for (f, t) in iter {
        *t = kernel(f);
    }
}
```

## Rust naive zipped

As with the Rust serial zipped version, the buffers are zipped and iterated but
this time in each spawned thread:

```rust
// ...
s.spawn(move || {
  let iter = std::iter::zip(from_part, to_part);
  for (f, t) in iter {
    *t = kernel(f);
  }
});
// ...
```

## Zig implementations

In this section I describe the versions of map I implemented with Zig.

## Zig serial

The Zig version (serial and parallel) have a similar interface for the `map`
function:

```zig
inline fn map(comptime U: type, comptime V: type, from: []const U, to: []V, kernel: fn (u: U) callconv(.Inline) V) void {
  // ...
}
```

* `U` defines the type of the `from` buffer (which is the third parameter)
* `V` defines the type of the `to` buffer (which is the fourth parameter)
* The fifth parameter is the `kernel` function that maps the elements of the
  `from` to the `to` buffer. I added the calling convention `.Inline` since I
  thought it may further optimize the execution (however I did not further
  evaluate this thought).

The implementation is also pretty obvious:

```zig
var i: usize = 0;
var len = @min(from.len, to.len);
while (i < len) : (i += 1) {
    to[i] = kernel(from[i]);
}
```

## Zig parallel

The parallel version has a slight different interface then the serial version:

```zig
inline fn map(comptime number_of_threads: usize, comptime U: type, comptime V: type, from: []const U, to: []V, kernel: fn (u: U) callconv(.Inline) V) !void {
  // ...
}
```

As additional (and first parameter) the number of to be used threads is
required. It is needed since I want to join all threads to finish the execution
of the map function. This means I need to store each thread and this requires
a data structure like a list or array. So either I would need an allocator to
create, for example, an `ArrayList` or I create an array with the size known at
compile time. (There may be further options, but I chose the array). It's not
the best interface but it suffices for now.

The implementation of the parallel `map` function is more involved as the
serial version.

```zig
inline fn map(comptime number_of_threads: usize, comptime U: type, comptime V: type, from: []const U, to: []V, kernel: fn (u: U) callconv(.Inline) V) !void {
    const len = @min(from.len, to.len);
    const len_per_thread = len / number_of_threads;
    var threads: [number_of_threads]Thread = undefined;
    for (threads) |*thread_handle, j| {
        const thread = try Thread.spawn(.{}, loopInThread, .{ U, V, from[j * len_per_thread .. (j + 1) * len_per_thread], to[j * len_per_thread .. (j + 1) * len_per_thread], kernel });
        thread_handle.* = thread;
    }
    // join all threads
    for (threads) |thread| {
        thread.join();
    }
}
```

At first the minimal length of both buffers (`to` and `from`) is calculated to
avoid the length of both buffers that are to be processed in each thread. As in
the Rust version I do not consider that the division `len / number_of_threads`
may result in a reminder that in non-zero, but adding this is possible.

After the `len_per_thread` is calculated, the array is created by using the
compile time known parameter `number_of_threads`. Then each thread is spawned
with the `loopInThread` function and the part of the `to` and the `from` buffer
this particular thread is about to process. Additionally each thread needs the
kernel function to execute the mapping. Each thread is stored in the `threads`
array which is then iterated to join all threads and finish the execution of
the `map` function.

The `loopInThread` function is again rather straight forward and pretty similar
to the serial version:

```zig
inline fn loopInThread(comptime U: type, comptime V: type, from: []const U, to: []V, kernel: fn (u: U) callconv(.Inline) V) void {
    var i: usize = 0;
    var len = @min(from.len, to.len);
    while (i < len) : (i += 1) {
        to[i] = kernel(from[i]);
    }
}
```

## Time measurements

Let me start with an important disclaimer: The time measurements I did are not
scientific and can not be used to draw wide-spread conclusions from them. For one
I did measure the time only once and not several times, nor did I calculate the
standard deviation and other parameters from the measurements (especially
since I only did one measurement) as [criterion](https://crates.io/crates/criterion)
does. I also did not change the problem size, in order to find the size at which
parallelization would be beneficial. I also did not investigate further (e.g.
looking at the assembly) or extended the scope of the measurements to get better
insights.

I did the measurements to get a feeling that the naive parallelization
implementations are faster than the serial versions for the chosen problem and
problem size. The time measurements also motivated the unsafe version I wrote in
Rust.

This means: take these time measurements with a big grain of salt: A fist sized
salt crystal ;)

I took the time measurements with the following circumstances:

* On my laptop: Intel(R) Core(TM) i5-2410M CPU @ 2.30GHz
* Rust versions were compiled with `--release`
* Rust compiler version: 1.68.0
* Zig versions were compiled in all three release modes:
  * `-Drelease-fast=true`
  * `-Drelease-safe=true`
  * `-Drelease-small=true`: As pointed out in Reddit, adding small to this comparison
    is not that useful.
* Zig compiler version: 0.10.1
* As mentioned above, the set up was mapping a 300 million sized `f32` buffer
  using the built-in square root functions as kernel (resp. mapping) function

![Execution time in ms of different map implementations](./execution_time.png "Execution time in ms of different map implementations")

As you can see, I ordered the measurements in a way, that they seem to form 4
groups:

* The first group is the Rust serial version and the Zig serial version
  compiled with `Drelease-small=true`. One could name this group as "serial
  safe", however the Zig serial version with `Drelease-safe=true` is not part
  of the group. This is what I would have expected, but other than that it
  makes sense that the Rust serial safe version and the Zig serial small
  version have similar execution time. As pointed out in Reddit,
  `Drelease-small=true`does not have safety checks enabled. 
* The next group contains Rust naive parallel version, the Zig parallel version
  compiled with `Drelease-small=true` and the Rust rayon version. All three of
  them have similar performance and all are faster then the serial safe versions.
  This group could be called "parallel safe". What I want to highlight especially
  is the performance of rayon. If you think of the flexibility and ergonomics and
  safety of rayon, this is just great. My implementations are very specific while
  rayon's implementation is very, very flexible. As mentioned before in the serial
  safe group: `Drelease-small=true` has safety checks disabled.
* The third group could be called "serial unsafe": It contains the Rust serial
  unsafe version and the Zig serial versions compiled with `Drelease-fast=true`
  and `Drelease-safe=true`. Why this version is as fast as the other - actually
  unsafe - versions is not clear to me. Also I double checked, there may be a
  mistake from my side. However I did not further investigate. What I also
  can only guess is why these serial versions are faster then the parallel
  safe version of group 2. If I would have to guess than I would say that the
  bounds checks that are performed in the safe parallel version could be the
  reason.
    * Due to the update I added the Rust serial zipped version to this group.
      However, it is completely safe.
* The last group consists of the Rust naive (parallel) unsafe version, Zig
  parallel version compiled with `Drelease-fast=true` and the Zig parallel
  version compiled with `Drelease-safe=true`. This group could be called
  "parallel unsafe" although the safe version of Zig parallel is contained.
  As before I did not expect this and I have no explanation (and did not
  try to find out, sorry). However that these versions are the fastest and
  faster than the versions in the "serial unsafe" group, which makes sense for
  me.
  * I also added the two Rust zipped versions here: Rust rayon zipped and Rust
    naive zipped. They are as fast as the other ones but these do not use
    unsafe.

To sum up, my measurements did reflect my expectation: If the problem size
is large enough, a parallel map implementation should be faster than a serial
version.

## Conclusion / what I learned

I enjoyed implementing these versions. I was surprised how straight-forward
the implementation could be done in Rust. I think, that it was in a small part
due to my Rust skill: I program Rust since mid 2019 and would consider myself
on an intermediate level (if there are such things as levels). However the
larger part goes to the Rust std lib, which adds such valuable functions like
`split_at_mut` and scoped threads. And especially the zip functions I just
learned.

I also enjoyed the Zig versions, where scoped threads do not exist but I could
also realize it with a simple array.

My naive versions are obviously not optimized nor very ergonomic (thinking about
comparing it with rayon here). For example I create new threads each time the
`map` function is called and join them at the end of the execution. In a real and
useful data parallel pipeline (ahem, rayon) reusing the threads between multiple
stages (e.g. several map stages and a final reduce stage) is beneficial since there
is an overhead when threads are created.

However I did learn a lot about both languages and data parallelism and I hope
you did enjoy the blog post.

Thank you for reading and as always, use the reddit link below if you want to
discuss, ask questions, correct errors, constructively criticize this post.

## Discussion

[Reddit - post](https://www.reddit.com/user/huntrss/comments/11u15q9/naive_parallel_map_implementation_in_rust_and_zig/)

